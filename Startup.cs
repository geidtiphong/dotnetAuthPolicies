using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace dotnetAuthPolicies {
  public class Startup {
    public Startup (IConfiguration configuration) {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices (IServiceCollection services) {
      services.AddControllers ();
      services.AddAuthentication (JwtBearerDefaults.AuthenticationScheme).AddJwtBearer (options => {
        options.TokenValidationParameters = new TokenValidationParameters () {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey (Encoding.UTF8.GetBytes ("http://example.com")),
        ValidateIssuer = false,
        ValidateAudience = false
        };
      });
      services.AddAuthorization (options => {
        options.AddPolicy ("CanEnterSecurity", policyBuilder => {
          policyBuilder.AuthenticationSchemes.Add (JwtBearerDefaults.AuthenticationScheme);
          policyBuilder.RequireClaim ("BoardingPassNumber", "1234");
        });
        // options.AddPolicy ("CanAccessLounge", policyBuilder => policyBuilder.AddRequirements (new MinimumAgeRequirement (18), new AllowedInLoungeRequirement ()));
        options.AddPolicy ("CanAccessLounge", policyBuilder => policyBuilder.AddRequirements (new AllowedInLoungeRequirement ()));
      });

      // services.AddSingleton<IAuthorizationHandler, MinimumAgeHandler>();
      services.AddSingleton<IAuthorizationHandler, FrequentFlyerHandler> ();
      services.AddSingleton<IAuthorizationHandler, IsAirlineEmployeeHandler> ();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }

      app.UseHttpsRedirection ();

      app.UseRouting ();

      app.UseAuthentication ();
      app.UseAuthorization ();

      app.UseEndpoints (endpoints => {
        endpoints.MapControllers ();
      });
    }
  }
}