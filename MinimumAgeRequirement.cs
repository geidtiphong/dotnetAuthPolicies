using System;
using Microsoft.AspNetCore.Authorization;

namespace dotnetAuthPolicies
{
    public class MinimumAgeRequirement : IAuthorizationRequirement
    {
      public int MinimumAge {get;}
       public MinimumAgeRequirement(int minimumAge) {
         MinimumAge = minimumAge;
       }
    }
}
