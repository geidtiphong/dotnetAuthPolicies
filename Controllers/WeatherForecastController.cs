﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace dotnetAuthPolicies.Controllers {
  [ApiController]
  [Route ("[controller]")]
  public class WeatherForecastController : ControllerBase {
    private static readonly string[] Summaries = new [] {
      "Freezing",
      "Bracing",
      "Chilly",
      "Cool",
      "Mild",
      "Warm",
      "Balmy",
      "Hot",
      "Sweltering",
      "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController (ILogger<WeatherForecastController> logger) {
      _logger = logger;
    }

    [Authorize("CanEnterSecurity")]
    // [Authorize("CanAccessLounge")]
    [HttpGet]
    public IEnumerable<WeatherForecast> Get () {
      var rng = new Random ();
      return Enumerable.Range (1, 5).Select (index => new WeatherForecast {
          Date = DateTime.Now.AddDays (index),
            TemperatureC = rng.Next (-20, 55),
            Summary = Summaries[rng.Next (Summaries.Length)]
        })
        .ToArray ();
    }

    [Authorize("CanAccessLounge")]
    [HttpGet("Lounge")]
    public string Lounge() {
      return "Welcome to lounge";
    }

    [AllowAnonymous]
    [HttpGet ("GenerateJSONWebToken")]
    public string GenerateJSONWebToken () {
      var securityKey = new SymmetricSecurityKey (Encoding.UTF8.GetBytes ("http://example.com"));
      var credentials = new SigningCredentials (securityKey, SecurityAlgorithms.HmacSha256);

      var claims = new [] {
        new Claim ("BoardingPassNumber", "1234"),
        new Claim (JwtRegisteredClaimNames.Jti, Guid.NewGuid ().ToString ())
      };

      var token = new JwtSecurityToken ("http://example.com",
        "http://example.com",
        claims,
        expires : DateTime.Now.AddHours (23),
        signingCredentials : credentials);

      return new JwtSecurityTokenHandler ().WriteToken (token);
    }
  }
}