using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace dotnetAuthPolicies
{
  public class FrequentFlyerHandler : AuthorizationHandler<AllowedInLoungeRequirement>
  {
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AllowedInLoungeRequirement requirement)
    {
      if (context.User.HasClaim("FrequentFlyerClass", "Gold"))
      {
          context.Succeed(requirement);
      }

      return Task.CompletedTask;
    }
  }
}